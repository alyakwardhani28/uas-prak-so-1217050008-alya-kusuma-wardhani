import streamlit as st

# Fungsi untuk mendeteksi bencana gunung meletus berdasarkan latitude dan longitude
def detect_volcano(latitude, longitude):
    # Logika deteksi bencana gunung meletus di sini
    # Contoh sederhana: Jika latitude < -6 dan longitude > 106, dianggap sebagai bencana gunung meletus
    if latitude < -6 and longitude > 106:
        return "Bencana Gunung Meletus"
    else:
        return "Tidak ada Bencana Gunung Meletus"

# Tampilan Streamlit
def main():
    st.title("Deteksi Bencana Gunung Meletus")
    
    # Input latitude dan longitude
    latitude = st.number_input("Latitude:")
    longitude = st.number_input("Longitude:")
    
    # Tombol untuk mendeteksi bencana gunung meletus
    if st.button("Deteksi"):
        # Memanggil fungsi detect_volcano
        result = detect_volcano(latitude, longitude)
        st.write("Hasil Deteksi:", result)

if __name__ == "__main__":
    main()
