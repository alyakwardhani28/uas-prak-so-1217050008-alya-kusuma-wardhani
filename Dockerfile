FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y git neovim netcat python3-pip

COPY volcano.py /home/volcano-detect/volcano.py
COPY requirements.txt /home/volcano-detect/requirements.txt

RUN pip3 install --no-cache-dir -r /home/volcano-detect/requirements.txt

WORKDIR /home/volcano-detect

EXPOSE 25008

CMD ["streamlit", "run", "--server.port=25008", "volcano.py"]